<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Authantication';
$route['404_override'] = '';
$route['users'] = 'user';
$route['invoice'] = 'dashboard/invoice';
$route['create-invoice'] = 'dashboard/create_invoice';
// $route['translate_uri_dashes'] = FALSE;
// $route['my-account'] = 'Authantication/index';
// $route['login'] = 'Authantication/adminIndex';
// $route['institute-home'] = 'home/institute_home';
// $route['create-course'] = 'Courses/createCourse';
// $route['create-category'] = 'home/createCategory';
// $route['courses/(:any)'] = 'Courses/index/$1';
// $route['courseStore'] = 'Courses/store';
// $route['course-search'] = 'Courses/setSession';
// $route['course-details/(:any)'] = 'Courses/course_detail/$1';
// $route['teachers'] = 'Teachers/index';
// $route['booked-courses'] = 'Courses/booked_course';
// $route['create-teacher'] = 'Teachers/createTeacher';
// $route['course-details/(:any)'] = 'Courses/course_detail/$1';
// $route['dashboard'] = 'admin/dashboard';

// $route['books-items'] = 'admin/Order/index';
// $route['students'] = 'admin/user/students';
// $route['experts'] = 'admin/user/experts';
// $route['new-courses'] = 'admin/courses';
// $route['approved-courses'] = 'admin/courses/approved_courses';
// $route['category'] = 'admin/Category';
// $route['site-info'] = 'setting/site_setting';
// $route['profile'] = 'Users';
// $route['contact-us'] = 'Home/contact_us';
// $route['notification'] = 'Notification/index';
// $route['cart'] = 'Cart/index';
// $route['checkout'] = 'Cart/checkout';



