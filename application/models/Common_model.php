<?php 
class Common_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

  public function get_site_info(){
    //$adminID = $this->session->userdata('user_type')=='Teacher' ? $this->session->userdata('id') : 1;
    $this->db->where('adminID', 1);
    return $this->db->get('site_info')->row();
  }

  public function get_about(){
   return $this->db->get('about_us')->row();
  }
  
  public function get_states()
  {
      return $this->db->get('states')->result();

  }

  public function get_cities()
  {
      return $this->db->get('cities')->result();
  }

  public function get_state_wise_city($stateID){
    $this->db->where('state_id',$stateID);
    return $this->db->get('cities')->result();
}

public function get_invoices($condition)
{
    $this->db->select('invoice.*,users.name as userName');
    $this->db->join('users', 'users.id = invoice.userID','left');
    $this->db->where($condition);
    return $this->db->get('invoice')->result();
}

public function get_invoice($condition)
{
    $this->db->select('invoice.*,users.name as userName,users.email as userEmail,users.contact as userContact,users.address as userAddress,users.pancardNo as userPancard,users.cgst as userCgst,users.sgst as userSgst,users.igst as userIGST,users.company_name as companyName,users.sac as userSAC,states.name as stateName,cities.city as cityName');
    $this->db->join('users', 'users.id = invoice.userID','left');
    $this->db->join('states', 'states.id = users.state','left');
    $this->db->join('cities', 'cities.id = users.city','left');
    $this->db->where($condition);
    return $this->db->get('invoice')->row();
}

public function last_invoices(){
  $this->db->select('invoiceNo');
  $this->db->order_by('id','desc');
  $this->db->limit(1);
  return $this->db->get('invoice');
}

public function store_invoice($data){
  return $this->db->insert('invoice', $data);
}

}
