
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title"><?=$page_title=='Institutes' ? '<i class="fa fa-institution"></i>' : '<i class="fa fa-users"></i>'?> <?=$page_title?></h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
								<li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>

		<!-- Main content -->
		<section class="content">
		  <div class="row">

					<div class="col-md-6 col-lg-6"> 
				  	<div class="box-header with-border">
						 <h3 class="box-title">All <?=$page_title?></h3>
					  </div>
				   </div>
				 <div class="col-md-6 col-lg-6 ">
					 <div class="box-header with-border">
					  <a href="<?=base_url('create-invoice')?>" class="btn btn-primary btn-sm float-right" >Add <?=$page_title?> <i class="fa fa-plus"></i></a>
					 </div>
				 </div>
				<!-- /.box-header -->
        <div class="box">
				<div class="box-body">
					<div class="table-responsive">
					  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
						<thead>
							<tr>
                <th>SNO</th>
                <th>Invoice ID</th>
                <th>User Name</th>
								<th>Customer Name</th>
								<th>Customer Email</th>
								<th>Customer Mobile</th>
								<th>Customer Address</th>
								<th>Ticket No</th>
								<th>Flight</th>
								<th>Created Date</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
                <?php foreach($invoices as $key=>$invoice){
                  //echo $invoice->id;
                  ?>
							<tr>
								<td><?=$key+1;?></td>
                <td><?= $invoice->invoiceNo?></td>
                <td><?= $invoice->userName?></td>
								<td><?= $invoice->customerName?></td>
								<td><?= $invoice->customerEmail?></td>
								<td><?= $invoice->customerContact?></td>
								<td><?= $invoice->customer_address?></td>
								<td><?= $invoice->ticketNo?></td>
								<td><?= $invoice->flight?></td>
                <td><?= date('d-m-Y',strtotime($invoice->created_at));?></td>
								<td><a  href="javascript:void(0);"   data-toggle="tooltip" title="Click to Change Status"><?= $invoice->status == 1 ? '<span class="btn  btn-success">Active</span>' : '<span class="btn btn-danger">De-Active</span>'?></a></td>
								<td>
									<a href="dashboard/invoice_report/<?= base64_encode($invoice->id)?>"  class="btn btn-primary btn-sm" data-toggle="tooltip" title="Invoice Report Download"><i class="fa fa-download"></i></a>
								
                </td>
							</tr>
              <?php } ?>
				
						</tbody>				  
					
					</table>
					</div>              
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->          
			</div>
			<!-- /.col -->
		  </div>
		  <!-- /.row -->
		</section>
		<!-- /.content -->
	  
	  </div>
  </div>
  <!-- /.content-wrapper -->

<!-- View Class Modal Start -->
<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('user/store')?>" id="addUserForm" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
      <div class="form-group">
            <label for="name" class="col-form-label">Company Name:</label>
            <input type="text" class="form-control" name="company_name" id="company_name">
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">Name:</label>
            <input type="text" class="form-control" name="name" id="name">
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">Email:</label>
            <input type="email" class="form-control" name="email" id="email">
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">Contact:</label>
            <input type="text" class="form-control" name="contact" id="contact" maxlength="10" minlength="10"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">Pancard No.:</label>
            <input type="text" class="form-control" name="pancardNo" id="pancardNo" >
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">CGST No.:</label>
            <input type="text" class="form-control" name="cgst" id="cgst" >
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">SGST No.:</label>
            <input type="text" class="form-control" name="sgst" id="sgst" >
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">IGST No.:</label>
            <input type="text" class="form-control" name="igst" id="igst" >
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">SAC.:</label>
            <input type="text" class="form-control" name="sac" id="sac" >
          </div>
          <div class="form-group">
            <label for="address" class="col-form-label">Address:</label>
            <textarea  class="form-control" name="address" id="addresss"> </textarea>
          </div>
      
          <div class="form-group">
            <label for="state" class="col-form-label">State:</label>
            <select class="form-control" name="state" id="state" onchange="getCity(this.value )">
              <option value="">Select State</option>
              <?php foreach($states as $state){?>
                <option value="<?=$state->id?>"><?=$state->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="city" class="col-form-label">City:</label>
            <select class="form-control" name="city" id="city">
              <option value="">Select City</option>
            </select>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- Edit User Modal End -->

<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?=base_url('user/update')?>" id="editUserForm" method="POST" enctype="multipart/form-data">
      <div class="modal-body" id="editUserModalData">
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- View Users -->
<div class="modal fade" id="viewUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">View User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="viewUserData">
  
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
    function getCity(stateID){
    $.ajax({
       url: '<?=base_url('Ajax_controller/get_city')?>',
       type: 'POST',
       data: {stateID},
       success: function (data) {
         $('#city').html(data);
         $('.city').html(data);
       }
     });
  }

  $("form#addUserForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  //$('.modal').modal('hide');
  //toastr.success(data.message);
    Swal.fire({
    icon: 'success',
    title: 'Success',
    text: data.message,
    //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
    setTimeout(function(){
      location.reload();
  }, 1000) 
  
  }else if(data.status==403) {
  //toastr.error(data.message);
     Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: data.message,
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }else{
      Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong',
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });

  $("form#editUserForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();    
  var formData = new FormData(this);
  $.ajax({
  url: $(this).attr('action'),
  type: 'POST',
  data: formData,
  cache: false,
  contentType: false,
  processData: false,
  dataType: 'json',
  success: function (data) {
  if(data.status==200) {
  //$('.modal').modal('hide');
  //toastr.success(data.message);
    Swal.fire({
    icon: 'success',
    title: 'Success',
    text: data.message,
    //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
    setTimeout(function(){
      location.reload();
  }, 1000) 
  
  }else if(data.status==403) {
  //toastr.error(data.message);
     Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: data.message,
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }else{
      Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong',
      //footer: '<a href="">Why do I have this issue?</a>'
    })
  $(':input[type="submit"]').prop('disabled', false);
  }
  },
  error: function(){} 
  });
  });
function updateUserStatus(user_id, status){
     var messageText  = "You want to "+status+" this user?";
     var confirmText =  'Yes, Change it!';
     var message  ="User status changed Successfully!";
    Swal.fire({
        title: 'Are you sure?',
        text: messageText,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmText
        }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '<?=base_url('user/update_status')?>', 
                method: 'POST',
                data: {userid: user_id, user_status: status},
                success: function(result){
                toastr.success(message);
                setTimeout(function(){
                   window.location.reload();
                }, 2000);
        }
      });
          
        }
        })
  }

  function viewModalShow(userid){
    $.ajax({
       url: '<?=base_url('user/viewUser')?>',
       type: 'POST',
       data: {userid},
       success: function (data) {
        $('#viewUserModal').modal('show');
         $('#viewUserData').html(data);
       }
     });
  }

  function editModalShow(userid){
    $.ajax({
       url: '<?=base_url('user/editUserForm')?>',
       type: 'POST',
       data: {userid},
       success: function (data) {
        $('#editUserModal').modal('show');
         $('#editUserModalData').html(data);
       }
     });
  }
</script>