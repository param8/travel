<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar position-relative">	
	  	<div class="multinav">
		  <div class="multinav-scroll" style="height: 100%; overflow:auto">	
			  <!-- sidebar menu-->
			  <ul class="sidebar-menu" data-widget="tree">	
				<li class="header">Dashboard</li>
				<li class="">
				  <a href="<?=base_url('dashboard')?>">
					<i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
					<span>Dashboard</span>
				</li>
		
				<li class="header">Components </li>
				<li >
				  <a href="<?=base_url('users')?>">
					<i class="fa fa-users"><span class="path1"></span><span class="path2"></span></i>
					<span>Users</span>
				</li>

				<li >
				  <a href="<?=base_url('invoice')?>">
					<i class="fas fa-file-invoice"><span class="path1"></span><span class="path2"></span></i>
					<span>Invoice</span>
				</li>

			  </ul>
		  </div>
		</div>
    </section>
	<div class="sidebar-footer">
		<a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Settings" aria-describedby="tooltip92529"><span class="icon-Settings-2"></span></a>
		<a href="mailbox.html" class="link" data-toggle="tooltip" title="" data-original-title="Email"><span class="icon-Mail"></span></a>
		<a href="<?=base_url('authantication/adminLogout')?>" class="link" data-toggle="tooltip" title="" data-original-title="Logout"><span class="icon-Lock-overturning"><span class="path1"></span><span class="path2"></span></span></a>
	</div>
  </aside>