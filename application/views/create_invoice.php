<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <div class="container-full">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title"><i class="fa fa-university"> <?=$page_title?></i></h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page"><?=$page_title?></li>
                <li class="breadcrumb-item active" aria-current="page"><?=$page_title?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-lg-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$page_title?></h3>
            </div>
          </div>
        </div>
        </div>
        <!-- /.box-header -->
        <div class="">
          <form action="<?=base_url('dashboard/store_invoice')?>" id="addInvoiceForm" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
            <div class="form-group">
                <label for="s_id" class="col-form-label">User Name:</label>
                <select class="form-control" name="userID" id="userID">
                  <option value="">Select User</option>
                  <?php foreach ($users as $user){?>
                    <option value="<?=$user->id?>" <?=$user->id==$this->session->userdata('id') ? 'selected' : ''?>><?=$user->name?></option>
                    <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label for="s_id" class="col-form-label">Customer Name:</label>
                <input type="text" class="form-control" name="customerName" id="customerName" > 
              </div>
              <div class="form-group">
                <label for="language_id" class="col-form-label">Customer Mobile No.:</label>
                <input type="text" class="form-control" maxlength="10" minlength="10" name="customerContact" id="customerContact"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"> 
              </div>
       
              <div class="form-group">
                <label for="remarks" class="col-form-label">Customer Email:</label>
                <input type="email" class="form-control" name="customerEmail" id="customerEmail" >
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Customer Address:</label>
                <textarea  class="form-control" name="customer_address" id="customer_address"></textarea>
              </div>
              <div class="form-group">
                <label for="customer_gst" class="col-form-label">Customer GST No:</label>
                <input type="text" class="form-control" name="customer_gst" id="customer_gst" >
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Ticket No:</label>
                <input type="text" class="form-control" name="ticketNo" id="ticketNo" >
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">PNR:</label>
                <input type="text" class="form-control" name="pnr" id="pnr" >
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Sectors:</label>
                <input type="text" class="form-control" name="sectors" id="sectors" >
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Flight:</label>
                <input type="text" class="form-control" name="flight" id="flight" >
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Type:</label>
                <input type="text" class="form-control" name="type" id="type" >
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Class:</label>
                <input type="text" class="form-control" name="class" id="class" >
              </div>
              <div class="form-group">
                <label for="fare" class="col-form-label">Fare:</label>
                <input type="text" class="form-control" name="fare" id="fare" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">OT Tax:</label>
                <input type="text" class="form-control" name="ot_tax" id="ot_tax" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">K3 GST:</label>
                <input type="text"  class="form-control" name="k3_gst" id="k3_gst" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">YQ Tax:</label>
                <input type="text" class="form-control" name="yq_tax" id="yq_tax" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">YR Tax:</label>
                <input type="text" class="form-control" name="yr_tax" id="yr_tax" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Bag Cahrges:</label>
                <input type="text" class="form-control" name="bag_cahrge" id="bag_cahrge" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Meal Charges:</label>
                <input type="text" class="form-control" name="meal_charge" id="meal_charge" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Seat Charges:</label>
                <input type="text" class="form-control" name="seat_charge" id="seat_charge" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
              <div class="form-group">
                <label for="remarks" class="col-form-label">Special Service Charges:</label>
                <input type="text" class="form-control" name="sp_service_charge" id="sp_service_charge" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
              <div class="form-group">
                <label for="fare" class="col-form-label">Service Charges:</label>
                <input type="text" class="form-control" name="service_charge" id="service_charge" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>

              <div class="form-group">
                <label for="fare" class="col-form-label">Globle PR Charges:</label>
                <input type="text" class="form-control" name="globle_pr_charge" id="globle_pr_charge" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              </div>

              <div class="form-group">
                <label for="s_id" class="col-form-label">Invoice Status:</label>
                <select class="form-control" name="invoice_status" id="invoice_status">
                  <option value="">Select Invoice Status</option>
                    <option value="Paid" >Paid</option>
                    <option value="Un Paid" >Un Paid</option>
                </select>
              </div>
             
            </div>
            <div class="modal-footer">
              <button type="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->          
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $("form#addInvoiceForm").submit(function(e) {
     $(':input[type="submit"]').prop('disabled', true);
     e.preventDefault();    
     var formData = new FormData(this);
     $.ajax({
       url: $(this).attr('action'),
       type: 'POST',
       data: formData,
       cache: false,
       contentType: false,
       processData: false,
       dataType: 'json',
       success: function (data) {
         if(data.status==200) {
           toastr.success(data.message);
  				setTimeout(function(){
                      location.reload();
           }, 1000) 
  
         }else if(data.status==403) {
           toastr.error(data.message);
           $(':input[type="submit"]').prop('disabled', false);
         }else{
           toastr.error('Unable to add site info');
           $(':input[type="submit"]').prop('disabled', false);
         }
       },
       error: function(){} 
     });
   });
</script>