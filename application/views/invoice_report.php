<!DOCTYPE html>
<html lang="en">
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5, user-scalable=1" name="viewport" />
    <meta name="csrf-token" content="AOon20Bb4mYyagwLmaMmirPQ5QGYejseiIYqDpoJ">
    <link href="https://fonts.googleapis.com/css?family=Poppins:ital,wght@0,400;0,500;0,600;0,700;1,400&amp;display=swap" rel="stylesheet">
    <!-- crasoul -->
    <!-- bbbb -->
    <style>
      table tr td{
      border: 1px solid #26237c;    padding: 5px;
      }
      .b_none tr td {
      border:none;
      }
    </style>
  </head>
  <body class="  " style="font-family: math;">
  <table class="table-responsive">
        <tr style="border: none; ">
          <td style="padding: 0;border: none;width:700px" colspan="3"><img src="<?=base_url('public/header.PNG')?>" alt="logo" style="width:100%"></td>
        </tr>
        <tr>
          <td >Invoice No- <?=$invoice->invoiceNo?> </td>
          <td >Invoice Date- <?=date('d-M-Y',strtotime($invoice->created_at))?> </td>
          <td >PNR- <?=$invoice->pnr?> </td>
        </tr>
        <tr colspan="3" style="border:1 px solid black!important">
          <td colspan="2"><span style="width: 300px; font-weight: bold;border:none">TBO Tek Limited  </span></td>
          <td><span style="width: 300px; font-weight: bold; border:none">Invoice  </span></td>
        </tr>
        <tr>
          <td colspan="2">
              <div  style="width: 300px;">
                      <p style="margin: 0;"><b>(Formerly TBO Tek Private Limited,<br>
                        and Tek Travels Private Limited)</b>
                        <br>
                        Regd Office: E-78, South Extn Part– I, New<br>
                        Delhi 110049<br>
                        Corp Off: Plot No 728, Udyog Vihar PhaseV,Gurgaon 122016<br>
                        Email: info@travelboutiqueonline.com<br>
                      <p style="margin: 0;">Web: www.travelboutiqueonline.com<br>
                        Phone: 0124-4998999,<br>
                        State: Haryana
                      </p>
                      <p style="margin: 0;"><b>GSTIN: 06AACCT6259K1ZZ<br>
                        CI Number: U74999DL2006PTC155233<br>
                        PAN : AACCT6259K
                        </b>
                      </p>
                    </div>
          </td>
          <td>
          <div style="width: 300px; text-align: right;">
                      <p><?=$invoice->userAddress?>
                      </p>
                      <p>
                        <b>Phone</b> : <?=$invoice->userContact?><br>
                        <b>State</b> : <?=$invoice->stateName?><br>
                        <b>PAN</b> :   <?=$invoice->userPancard?>
                      </p>
                    </div>
          </td>
        </tr>        
      </table>
      <table  style=" border-spacing: 0;">
              <tr style="width:700px">
                <td><b class="t_h" style="font-size: 10px;">S  No. </b></td>
                <td><b class="t_h" style="font-size: 10px;">Ticket  No  </b></td>
                <td><b class="t_h" style="font-size: 10px;">Sectors </b></td>
                <td><b class="t_h" style="font-size: 10px;">Flight</b></td>
                <td> <b class="t_h" style="font-size: 10px;">PAX Name </b></td>
                <td> <b class="t_h" style="font-size: 10px;">Type     </b></td>
                <td> <b class="t_h" style="font-size: 10px;">Class </b></td>
                <td> <b class="t_h" style="font-size: 10px;">Fare </b></td>
                <td> <b class="t_h" style="font-size: 10px;">OT Tax </b></td>
                <td> <b class="t_h" style="font-size: 10px;">K3/GST </b></td>
                <td> <b class="t_h" style="font-size: 10px;">YQ Tax </b></td>
                <td> <b class="t_h" style="font-size: 10px;"> YR   Tax   </b></td>
                <td> <b class="t_h" style="font-size: 10px;">Bag.Ch. </b></td>
                <td> <b class="t_h" style="font-size: 10px;">Meal   Ch. </b></td>
                <td> <b class="t_h" style="font-size: 10px;">Seat  Ch.     </b> </td>
                <td> <b class="t_h" style="font-size: 10px;">Sp   Service  Ch. </b></td>
                <td> <b class="t_h" style="font-size: 10px;">Service Charges </b></td>
                <td> <b class="t_h" style="font-size: 10px;">Global   Pr.   Ch. </b></td>
              </tr>
              <tr>
                <td><b class="t_h" style="font-size: 10px;">1 </b></td>
                <td><b class="t_h" style="font-size: 10px;"><?=$invoice->ticketNo?>  </b></td>
                <td><b class="t_h" style="font-size: 10px;"><?=$invoice->sectors?> </b></td>
                <td><b class="t_h" style="font-size: 10px;"><?=$invoice->flight?></b></td>
                <td> <b class="t_h" style="font-size: 10px;"><?=$invoice->customerName?> </b>
                </td>
                <td> <b class="t_h" style="font-size: 10px;"><?=$invoice->type?>     </b></td>
                <td> <b class="t_h" style="font-size: 10px;"><?=$invoice->class?> </b></td>
                <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->fare)?> </b></td>
                <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->ot_tax)?> </b></td>
                <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->k3_gst)?>  </b></td>
                <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->yq_tax)?>  </b></td>
                <td> <b class="t_h" style="font-size: 10px;"> <?=number_format($invoice->yr_tax)?>    </b></td>
                <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->bag_cahrge)?> </b></td>
                <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->meal_charge)?> </b></td>
                <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->seat_charge)?>     </b> </td>
                <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->sp_service_charge)?> </b></td>
                <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->service_charge)?> </b></td>
                <td> <b class="t_h" style="font-size: 10px;"><?=number_format($invoice->globle_pr_charge)?>  </b></td>
              </tr>
    </table>
   </body>
</html>