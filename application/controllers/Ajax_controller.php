<?php 
class Ajax_controller extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
    //$this->not_admin_logged_in();

	}

	public function get_city()
    {
       $stateID =  $this->input->post('stateID');
       $cities = $this->Common_model->get_state_wise_city($stateID);
       if(count($cities) > 0)
       {
        ?>
        <option value="">Select City</option>
        <?php foreach($cities as $city){ ?>
           <option value="<?=$city->id?>" <?=$this->session->userdata('location_city')==$city->id ? 'selected' : ''?>><?=$city->city?></option>
        <?php
        }
       }else
       {
        ?>
        <option value="">No City found</option>
        <?php
       }
    }
}