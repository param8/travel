<?php 
class User extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_admin_logged_in();
		$this->load->model('user_model');
	}

	public function index()
	{	$data['page_title'] = 'Users';
        $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'User'));
		$this->admin_template('users',$data);
	}

   public function store(){
    $company_name = $this->input->post('company_name');
	$name = $this->input->post('name');
	$email = $this->input->post('email');
	$contact = $this->input->post('contact');
	$address = $this->input->post('address');
	$state = $this->input->post('state');
	$city = $this->input->post('city');
	$pancardNo = $this->input->post('pancardNo');
	$cgst = $this->input->post('cgst');
	$sgst = $this->input->post('sgst');
	$igst = $this->input->post('igst');
  $sac = $this->input->post('sac');

	if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
		exit();
	}
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
		exit();
	}
	$checkEmail = $this->user_model->get_user(array('email'=>$email));
	if($checkEmail){
		echo json_encode(['status'=>403,'message'=>'This email is already in use']);
		exit();
	}

	if(empty($contact)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
		exit();
	}

  if(empty($pancardNo)){
		echo json_encode(['status'=>403, 'message'=>'Please enter pancard']); 	
		exit();
	}
 
	if(empty($address)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your address']); 	
		exit();
	}

	if(empty($state)){
		echo json_encode(['status'=>403, 'message'=>'Please select state']); 	
		exit();
	}
	if(empty($city)){
		echo json_encode(['status'=>403, 'message'=>'Please select city']); 	
		exit();
	}
	
	$data = array(
    'company_name'=> $company_name,
		'name'        => $name,
		'email'       => $email,
		'password'    => md5($contact),
		'contact'     => $contact ,
		'address'     => $address,
		'user_type'   => 'User',
		'state'       => $state,
		'city'        => $city,
    'pancardNo'   => $pancardNo,
		'cgst'        => $cgst,
		'sgst'        => $sgst,
		'igst'        => $igst,
    'sac'        => $sac,
	);
	$register = $this->user_model->store_user($data);

	if($register){
		echo json_encode(['status'=>200, 'message'=>'User register successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}

   }

	public function update_status()
	{
		$user_id = $this->input->post('userid');
		$user_status = ($this->input->post('user_status')=='Active')?'1':'0';

        $userdata =  array(
          'status' => $user_status
        );
	
        $update_status =  $this->user_model->update_user_status($userdata,array('id'=>$user_id));
   
	}

  public function editUserForm(){
    $userid = $this->input->post('userid');
    $states = $this->stateinfo();
		$user = $this->user_model->get_user_details(array('users.id'=>$userid));
    ?>
          <div class="form-group">
            <label for="name" class="col-form-label">Name:</label>
            <input type="text" class="form-control" name="name" id="name" value="<?=$user->name?>">
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">Email:</label>
            <input type="email" class="form-control"  value="<?=$user->email?>" readonly>
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">Contact:</label>
            <input type="text" class="form-control" name="contact" id="contact" value="<?=$user->contact?>" maxlength="10" minlength="10"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">Pancard No.:</label>
            <input type="text" class="form-control" name="pancardNo" id="pancardNo" value="<?=$user->pancardNo?>" >
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">CGST No.:</label>
            <input type="text" class="form-control" name="cgst" id="cgst" value="<?=$user->cgst?>">
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">SGST No.:</label>
            <input type="text" class="form-control" name="sgst" id="sgst" value="<?=$user->sgst?>">
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">IGST No.:</label>
            <input type="text" class="form-control" name="igst" id="igst" value="<?=$user->igst?>">
          </div>
          <div class="form-group">
            <label for="address" class="col-form-label">Address:</label>
            <textarea  class="form-control" name="address" id="addresss"><?=$user->address?></textarea>
          </div>
      
          <div class="form-group">
            <label for="state" class="col-form-label">State:</label>
            <select class="form-control" name="state" id="state" onchange="getCity(this.value )">
              <option value="">Select State</option>
              <?php foreach($states as $state){?>
                <option value="<?=$state->id?>" <?=$user->state==$state->id ? 'selected' : ''?>><?=$state->name?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="city" class="col-form-label">City:</label>
            <select class="form-control city" name="city" id="city">
              <option value="<?=$user->city?>"><?=$user->cityName?></option>
            </select>
          </div>
        
    <?php
  }


  public function update(){
  $id = $this->input->post('id');
  $name = $this->input->post('name');
	$contact = $this->input->post('contact');
	$address = $this->input->post('address');
	$state = $this->input->post('state');
	$city = $this->input->post('city');
	$pancardNo = $this->input->post('pancardNo');
	$cgst = $this->input->post('cgst');
	$sgst = $this->input->post('sgst');
	$igst = $this->input->post('igst');
  $user = $this->user_model->get_user_details(array('users.id'=>$id));
	if(empty($name)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
		exit();
	}
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
		exit();
	}

	if(empty($contact)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
		exit();
	}

  if(empty($pancardNo)){
		echo json_encode(['status'=>403, 'message'=>'Please enter pancard']); 	
		exit();
	}
 
	if(empty($address)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your address']); 	
		exit();
	}

	if(empty($state)){
		echo json_encode(['status'=>403, 'message'=>'Please select state']); 	
		exit();
	}
	if(empty($city)){
		echo json_encode(['status'=>403, 'message'=>'Please select city']); 	
		exit();
	}
	
	
	$data = array(
		'name'        => $name,
		'contact'     => $contact ,
		'address'     => $address,
		'state'       => $state,
		'city'        => $city,
    'pancardNo'   => $pancardNo,
		'cgst'        => $cgst,
		'sgst'        => $sgst,
		'igst'        => $igst,
	);
	$update = $this->user_model->update_user_status($data,array('id' => $id));

	if($update){
		echo json_encode(['status'=>200, 'message'=>'User register successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
	}
  }

	public function viewUser(){
		$userid = $this->input->post('userid');
		$user = $this->user_model->get_user_details(array('users.id'=>$userid));
		?>
			<div class="box mb-0">
				<div class="box-body box-profile">            
					<div class="row">
						<div class="col-12">
							<h4 class="box-inverse p-2">Personal Information</h4>
							<div>
								<p><strong>Name</strong> :<span class="text-gray pl-10"><?=$user->name;?></span> </p>
								<p><strong>Email</strong> :<span class="text-gray pl-10"><?=$user->email;?></span> </p>
								<p><strong>Phone</strong> :<span class="text-gray pl-10"><?=$user->contact;?></span></p>
								<p><strong>Address</strong> :<span class="text-gray pl-10"><?=$user->address;?> </span></p>
								<p><strong>State</strong> :<span class="text-gray pl-10"><?=$user->stateName;?></span></p>
								<p><strong>City</strong> :<span class="text-gray pl-10"><?=$user->cityName;?></span></p>
							</div>
							
							
						</div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
		<?php
	  }

	
}