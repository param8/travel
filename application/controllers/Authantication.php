<?php 
class Authantication extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		//$this->not_admin_logged_in();
		$this->load->model('Auth_model');
		$this->load->model('User_model');
        $this->load->model('setting_model');
	}


	public function index()
	{	
		$data['page_title'] = 'Login';
		$this->load->view('layout/login_head',$data);
		$this->load->view('login');
		$this->load->view('layout/login_footer');

	}

	public function login(){
		 $email = $this->input->post('email');
		$password = $this->input->post('password');
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email or contact']); 	
			exit();
		}
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
			exit();
		}
		$login = $this->Auth_model->login($email,$password);

		if($login==403){
			echo json_encode(['status'=>403, 'message'=>'email or password incorrect!']);
		}elseif($login==302){
			echo json_encode(['status'=>403, 'message'=>'Your are not active please contact the administrator']);   
		}else{
      echo json_encode(['status'=>200, 'message'=>'Login Successfully','user_type'=>$this->session->userdata('user_type')]);
    }
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('home', 'refresh');
	}


	public function register(){
    $siteinfo=$this->siteinfo();
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$contact = $this->input->post('contact');
		$address= $this->input->post('address');
		$user_type = $this->input->post('user_type');
		$state = $this->input->post('state');
		$city = $this->input->post('city');

		if(empty($name)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your name']); 	
			exit();
		}
		if(empty($email)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
			exit();
		}
		$checkEmail = $this->User_model->get_user(array('email'=>$email));
		if($checkEmail){
			echo json_encode(['status'=>403,'message'=>'This email is already in use']);
			exit();
		}
		if(empty($password)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
			exit();
		}
		if(empty($contact)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your mobile']); 	
			exit();
		}
    if(empty($user_type)){
			echo json_encode(['status'=>403, 'message'=>'Please select a user type']); 	
			exit();
		}
		if(empty($address)){
			echo json_encode(['status'=>403, 'message'=>'Please enter your address']); 	
			exit();
		}

		if(empty($state)){
			echo json_encode(['status'=>403, 'message'=>'Please select state']); 	
			exit();
		}
		if(empty($city)){
			echo json_encode(['status'=>403, 'message'=>'Please select city']); 	
			exit();
		}
    $this->load->library('upload');
    if($_FILES['profile_pic']['name'] != '')
		{
    $config = array(
      'upload_path' 	=> 'uploads/users',
      'file_name' 	=> str_replace(' ','',$name).uniqid(),
      'allowed_types' => 'jpg|jpeg|png|gif',
      'max_size' 		=> '10000000',
    );
        $this->upload->initialize($config);
    if ( ! $this->upload->do_upload('profile_pic'))
      {
          $error = $this->upload->display_errors();
          echo json_encode(['status'=>403, 'message'=>$error]);
          exit();
      }
      else
      {
        $type = explode('.',$_FILES['profile_pic']['name']);
        $type = $type[count($type) - 1];
        $image = 'uploads/users/'.$config['file_name'].'.'.$type;
      }
    }else{
      $image = 'assets/dummy_image.jpg';
    }
		$data = array(
			'name'        => $name,
			'email'       => $email,
			'password'    => md5($password),
			'contact'     => $contact ,
			'address'     => $address,
			'user_type'   => $user_type,
			'state'       => $state,
			'city'        => $city,
      'profile_pic' => $image,
		);
		$register = $this->Auth_model->register($data);

		if($register){
      if($user_type=='Institute'){
        $data = array(
          'adminID'        => $register,
          'site_name'      => $name,
		  'site_email'     => $email,
          'site_contact'   => $contact ,
          'site_address'   => $address ,
          'site_logo'      => $siteinfo->site_logo,
          'footer_contant' => $siteinfo->footer_contant,
        );
        $this->setting_model->store_siteInfo($data);
      }
          //$site_name  =   $this->input->post('site_name');
          $subject  =   "Welcome message " ;
          $html = "<h1> Hi ".$name." welcome... </h1>
          <p>Your user name is : ".$email ." and password is ".$password."  </p>";
          $sendmail = sendEmail($email,$subject,$html);
			echo json_encode(['status'=>200, 'message'=>'User register successfully!']);
		}else{
			echo json_encode(['status'=>302, 'message'=>'Invalid username or password!']);   
		}
	}
//   Admin Cradinciales

public function adminIndex()
{	$data['page_title'] = 'Login';
	$data['siteinfo'] = $this->siteinfo();
	$this->load->view('admin/layout/login_head',$data);
	$this->load->view('admin/login');
	$this->load->view('admin/layout/login_footer');
}

public function adminLogin(){
	//$this->logged_in();
	 $email = $this->input->post('username');
	 $password = $this->input->post('password');
	 $type = 'Admin';
	if(empty($email)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your email address']); 	
		exit();
	}
	if(empty($password)){
		echo json_encode(['status'=>403, 'message'=>'Please enter your password']); 	
		exit();
	}
	$login = $this->Auth_model->login($email,$password);

	if($login){
		echo json_encode(['status'=>200, 'message'=>'Login successfully!']);
	}else{
		echo json_encode(['status'=>302, 'message'=>'Invalid username or password!']);   
	}
}

public function adminLogout()
{
	$this->session->sess_destroy();
	redirect('login', 'refresh');
}
	
	
}