<?php 
class Dashboard extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_admin_logged_in();
		$this->load->model('auth_model');
		$this->load->model('user_model');
	}

	public function index()
	{	 
		//print_r($this->session->userdata());die;
		 $data['page_title'] = 'Dashboard';
		 $data['users'] = $this->user_model->get_all_users(array('users.user_type' => 'Users'));
	  $this->admin_template('dashboard',$data);
		
	}

  public function invoice(){
    $data['page_title'] = 'Invoice';
    $condition = $this->session->userdata('user_type')!='Admin' ? array('userID'=>$this->session->userdata('id')) : array('invoice.status'=>1);
		$data['invoices'] = $this->Common_model->get_invoices($condition);
	  $this->admin_template('invoice',$data);
  }

	public function create_invoice(){
    $data['page_title'] = 'Invoice';
    $condition = $this->session->userdata('user_type')!='Admin' ? array('id'=>$this->session->userdata('id')) : array('users.user_type' => 'User');
    $data['users'] = $this->user_model->get_all_users($condition);
	  $this->admin_template('create_invoice',$data);
  }

  public function store_invoice(){
    $userID = $this->input->post('userID');
    $customerName = $this->input->post('customerName');
    $customerEmail = $this->input->post('customerEmail');
    $customerContact = $this->input->post('customerContact');
    $customer_address = $this->input->post('customer_address');
    $customer_gst = $this->input->post('customer_gst');
    $ticketNo = $this->input->post('ticketNo');
    $pnr = $this->input->post('pnr');
    $sectors = $this->input->post('sectors');
    $flight = $this->input->post('flight');
    $type = $this->input->post('type');
    $class = $this->input->post('class');
    $fare = $this->input->post('fare');
    $ot_tax = $this->input->post('ot_tax');
    $k3_gst = $this->input->post('k3_gst');
    $yq_tax = $this->input->post('yq_tax');
    $yr_tax = $this->input->post('yr_tax');
    $bag_cahrge = $this->input->post('bag_cahrge');
    $meal_charge = $this->input->post('meal_charge');
    $seat_charge = $this->input->post('seat_charge');
    $sp_service_charge = $this->input->post('sp_service_charge');
    $service_charge = $this->input->post('service_charge');
    $globle_pr_charge = $this->input->post('globle_pr_charge');
    $invoice_status = $this->input->post('invoice_status');
    $invoice = $this->Common_model->last_invoices();

    if(empty($userID)){
      echo json_encode(['status'=>403, 'message'=>'Please select user']); 	
      exit();
    }
    if(empty($customerName)){
      echo json_encode(['status'=>403, 'message'=>'Please enter customer name']);  	
      exit();
    }

    if(empty($customerContact)){
      echo json_encode(['status'=>403, 'message'=>'Please enter customer contact']);  	
      exit();
    }
  
    if(empty($customerEmail)){
      echo json_encode(['status'=>403, 'message'=>'Please enter customer email']);  	
      exit();
    }
   
    if(empty($ticketNo)){
      echo json_encode(['status'=>403, 'message'=>'Please enter ticket no']); 	
      exit();
    }
  
    if(empty($pnr)){
      echo json_encode(['status'=>403, 'message'=>'Please enter pnr']); 	
      exit();
    }
    if(empty($sectors)){
      echo json_encode(['status'=>403, 'message'=>'Please enter sectors']);  	
      exit();
    }

    if(empty($flight)){
      echo json_encode(['status'=>403, 'message'=>'Please enter flight']);   	
      exit();
    }
    if(empty($type)){
      echo json_encode(['status'=>403, 'message'=>'Please enter type']);   	
      exit();
    }
    if(empty($class)){
      echo json_encode(['status'=>403, 'message'=>'Please enter class']);   	
      exit();
    }
    if(empty($fare)){
      echo json_encode(['status'=>403, 'message'=>'Please enter fare']);   	
      exit();
    }
    if(empty($invoice_status)){
      echo json_encode(['status'=>403, 'message'=>'Please select invoice status']);   	
      exit();
    }
     $invoiceNo = $invoice->num_rows() > 0 ? str_pad($invoice->row()->invoiceNo+1, 7, "0", STR_PAD_LEFT) : str_pad('0000101', 5, "0", STR_PAD_LEFT);
    $data =array(
      'userID'           => $userID,
      'invoiceNo'        => $invoiceNo,
      'customerName'     => $customerName,
      'customerEmail'    => $customerEmail ,
      'customerContact'  => $customerContact,
      'customer_address' => $customer_address,
      'customer_gst'     => $customer_gst,
      'ticketNo'         => $ticketNo,
      'pnr'              => $pnr,
      'sectors'          => $sectors,
      'flight'           => $flight,
      'type'             => $type,
      'class'            => $class,
      'fare'             => $fare,
      'ot_tax'           => $ot_tax,
      'k3_gst'           => $k3_gst,
      'yq_tax'           => $yq_tax ,
      'yr_tax'           => $yr_tax,
      'bag_cahrge'       => $bag_cahrge,
      'meal_charge'      => $meal_charge,
      'seat_charge'      => $seat_charge,
      'sp_service_charge'=> $sp_service_charge,
      'service_charge'   => $service_charge,
      'globle_pr_charge' => $globle_pr_charge,
      'invoice_status'   => $invoice_status,
    );

    $store = $this->Common_model->store_invoice($data);
    if($store){
      echo json_encode(['status'=>200, 'message'=>'Invoice added successfully!']);
    }else{
      echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
    }
  }

  public function invoice_report(){
    $invoiceID = base64_decode($this->uri->segment(3));
    $data['invoice'] = $this->Common_model->get_invoice(array('invoice.id'=>$invoiceID));
    //$this->load->view('invoice_report', $data);
    $this->load->library('pdf');
   $html = $this->load->view('invoice_report', $data, true);
    $this->pdf->createPDF($html, 'mypdf', false);
  }

}