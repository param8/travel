<?php
    
    function generate_app_no($last_id)
    {
        $prefix = 'VI'.date('ymd');
        return (strlen($last_id) >= 4) ? $prefix.$last_id : $prefix.str_pad($last_id, 4, '0', STR_PAD_LEFT);
    }

    function getItemNameByItemId($item_id)
    {
        $CI =& get_instance();
        $CI->db->from('school_class_items');
        $CI->db->where(array('id' => $item_id));
        $result= $CI->db->get()->row()->title;
        return $result;
    }
?>